/* acconfig.h - template used by autoheader for config.h.in */

/* package options handled by configure */
#define PACKAGE "nighthawk"
#define VERSION "0.0"

/* sprite option handled by configure */
#undef REDUCED_SPRITES

/* sound options handled by configure */
#define DEFAULT_SAM_RATE  22050
#define DEFAULT_PRECISION 16
#define DEFAULT_STEREO    1
#define NO_FX_CHANNELS    8
